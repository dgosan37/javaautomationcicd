package packetProcessorUnits.interfaces;

public interface SenderInterface {

    void send(byte[] encodedResponse) throws Exception;

}
